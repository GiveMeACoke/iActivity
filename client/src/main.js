import Vue from 'vue'
import App from './App.vue'
import router from './router'
import MintUI from 'mint-ui'
import {Upload} from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import '../public/font-awesome/css/font-awesome.min.css'
import 'mint-ui/lib/style.css'
// import rem from '../config/rem'
import api from './api/install'
import store from './store'
import '@/permission'
Vue.use(api)

Vue.use(MintUI)
Vue.use(Upload)
Vue.config.productionTip = false

//全局方法
Vue.mixin({
  methods:{
    getAuthHeaders(){
        return {
            Authorization: localStorage.getItem('Token') || ''
        }
    }
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
