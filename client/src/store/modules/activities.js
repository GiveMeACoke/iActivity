const activities = {
    namespaced: true,//开启命名空间
    state: {
        activityInfo:''
    },
    mutations: {
        SET_ACTIVITYINFO: (state, activityInfo) => {
            state.activityInfo = activityInfo
        },
    },
}

export default activities